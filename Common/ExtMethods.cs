﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Common
{
  /// <summary>
  /// 扩展方法
  /// </summary>
  public static class ExtMethods
  {
    #region 中文大写货币值

    /// <summary>
    /// 将数字格式化成中文
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public static string ToChineseString(this string number)
    {
      string chineseNum = fun_numtostr(Convert.ToDecimal(number));
      return chineseNum;
    }
    ///   <summary>   
    ///   把数字转换成大写格式,参数decimal，返回string   
    ///   </summary>   
    ///   <param   name="num">decimal   数字</param>   
    ///   <returns>数字大写</returns>   
    private static string fun_numtostr(decimal lnum)
    {
      decimal dnum = decimal.Parse(lnum.ToString().Trim());
      string str1 = "零壹贰叁肆伍陆柒捌玖";                         //0-9所对应的汉字     
      string str2 = "万仟佰拾亿仟佰拾万仟佰拾元角分";   //数字位所对应的汉字     
      string str3 = "";         //从原num值中取出的值     
      string str4 = "";         //数字的字符串形式     
      string str5 = "";     //人民币大写金额形式     
      int i;         //循环变量     
      int j;         //num的值乘以100的字符串长度     
      string ch1 = "";         //数字的汉语读法     
      string ch2 = "";         //数字位的汉字读法     
      int nzero = 0;     //用来计算连续的零值是几个     
      int temp;                         //从原num值中取出的值     

      dnum = Math.Round(Math.Abs(dnum), 2);         //将num取绝对值并四舍五入取2位小数     
      str4 = ((long)(dnum * 100)).ToString();                 //将num乘100并转换成字符串形式     
      j = str4.Length;             //找出最高位     
      if (j > 15) { return "溢出"; }
      str2 = str2.Substring(15 - j);       //取出对应位数的str2的值。如：200.55,j为5所以str2=佰拾元角分     

      //循环取出每一位需要转换的值     
      for (i = 0; i < j; i++)
      {
        str3 = str4.Substring(i, 1);                     //取出需转换的某一位的值     
        temp = Convert.ToInt32(str3);             //转换为数字     
        if (i != (j - 3) && i != (j - 7) && i != (j - 11) && i != (j - 15))
        {
          //当所取位数不为元、万、亿、万亿上的数字时     
          if (str3 == "0")
          {
            ch1 = "";
            ch2 = "";
            nzero = nzero + 1;
          }
          else
          {
            if (str3 != "0" && nzero != 0)
            {
              ch1 = "零" + str1.Substring(temp * 1, 1);
              ch2 = str2.Substring(i, 1);
              nzero = 0;
            }
            else
            {
              ch1 = str1.Substring(temp * 1, 1);
              ch2 = str2.Substring(i, 1);
              nzero = 0;
            }
          }
        }
        else
        {
          //该位是万亿，亿，万，元位等关键位     
          if (str3 != "0" && nzero != 0)
          {
            ch1 = "零" + str1.Substring(temp * 1, 1);
            ch2 = str2.Substring(i, 1);
            nzero = 0;
          }
          else
          {
            if (str3 != "0" && nzero == 0)
            {
              ch1 = str1.Substring(temp * 1, 1);
              ch2 = str2.Substring(i, 1);
              nzero = 0;
            }
            else
            {
              if (str3 == "0" && nzero >= 3)
              {
                ch1 = "";
                ch2 = "";
                nzero = nzero + 1;
              }
              else
              {
                if (j >= 11)
                {
                  ch1 = "";
                  nzero = nzero + 1;
                }
                else
                {
                  ch1 = "";
                  ch2 = str2.Substring(i, 1);
                  nzero = nzero + 1;
                }
              }
            }
          }
        }
        if (i == (j - 11) || i == (j - 3))
        {
          //如果该位是亿位或元位，则必须写上     
          ch2 = str2.Substring(i, 1);
        }
        str5 = str5 + ch1 + ch2;

        if (i == j - 1 && str3 == "0")
        {
          //最后一位（分）为0时，加上“整”     
          str5 = str5 + '整';
        }
      }
      if (dnum == 0)
      {
        str5 = "零元整";
      }
      return str5;
    }

    #endregion

    #region Object 数据转换
    public static decimal? ToNullableDecimal(this object obj)
    {
      decimal r = 0;
      if (obj != null)
      {
        if (decimal.TryParse(obj.ToString(), out r))
        {
          return r;
        }
        else
          return null;
      }
      else
        return null;
    }

    public static decimal ToDecimal(this object obj)
    {
      decimal r = 0;
      if (obj != null)
      {
        decimal.TryParse(obj.ToString(), out r);
      }
      return r;
    }

    public static string ToRequestString(this object obj)
    {
      if (obj == null)
      {
        return "";
      }
      else
      {
        return obj.ToString();
      }
    }

    public static int ToInt32(this object obj)
    {
      int r = 0;
      if (obj != null)
      {
        int.TryParse(obj.ToString(), out r);
      }
      return r;
    }

    public static string ToOracleDBString(this object obj)
    {
      string str = "null";
      if (obj != null)
      {
        if (obj.ToString().Trim() != "")
          str = "'" + obj.ToString() + "'";
        else
          str = "null";
      }
      return str;
    }

    /// <summary>
    /// 向上取整
    /// </summary>
    /// <param name="d"></param>
    /// <returns></returns>
    public static int Ceiling(this decimal d)
    {
      return Math.Ceiling(Convert.ToDecimal(d)).ToInt32();
    }

    /// <summary>
    ///向下取整
    /// </summary>
    /// <param name="d"></param>
    /// <returns></returns>
    public static int Floor(this decimal d)
    {
      return Math.Floor(Convert.ToDecimal(d)).ToInt32();
    }

    public static int? ToNullableInt32(this object obj)
    {
      int r = 0;
      if (obj != null)
      {
        if (int.TryParse(obj.ToString(), out r))
        {
          return r;
        }
        else
        {
          return null;
        }
      }
      else
      {
        return null;
      }
    }

    #endregion

    #region String 数据转换
    public static string CleanString(this string newStr)
    {

      string tempStr = newStr.Replace((char)13, (char)0);

      tempStr = tempStr.Replace((char)10, (char)0);

      return tempStr.Trim();

    }

    /// <summary>
    /// 通过char将List连接成字符串
    /// </summary>
    /// <param name="list"></param>
    /// <param name="ch"></param>
    /// <returns></returns>
    public static string ToJoinString(this IList<string> list, char ch)
    {
      string str = "";
      foreach (var q in list)
      {
        str += q + ch;
      }
      if (str.Length > 2)
        str = str.Remove(str.Length - 1);
      return str;
    }
    #endregion

    public static DataTable SortBy(this System.Data.DataTable dt, string sortName, string order)
    {
      try
      {
        DataView dv = dt.DefaultView;
        if (!string.IsNullOrEmpty(sortName))
        {
          dv.Sort = sortName + " " + order;
          return dv.ToTable();
        }
        else
        {
          return dt;
        }
      }
      catch
      {
        return dt;
      }

    }

    public static string ToHtml(this System.Data.DataTable dt)
    {
      StringBuilder sb = new StringBuilder();
      sb.Append("<table class=\"easyui-datagrid\" id=\"tt\" data-options=\"rownumbers:true\" style=\"height:300px;\">  ");
      sb.Append("<thead> ");
      string indexrow = "";
      string titlerow = "";
      string fieldrow = "";
      int i = 0;
      foreach (DataColumn dc in dt.Columns)
      {
        indexrow += "<th style='width:80;background-color:white'>" + i.ToString() + "</th>";
        fieldrow += "<th data-options=\"field:'" + dc.ColumnName + "',width:80,\">" + dc.ColumnName + "</th>";
        titlerow += "<th style='width:150'> </th>";
        i++;
      }
      //sb.Append("<tr>"+titlerow+"</tr>");
      //sb.Append("<tr>" + indexrow + "</tr>");
      sb.Append("<tr>" + fieldrow + "</tr>");
      sb.Append("</thead>");
      sb.Append("<tbody> ");
      foreach (DataRow dr in dt.Rows)
      {
        sb.Append("<tr>");
        for (i = 0; i < dt.Columns.Count; i++)
        {
          sb.Append("<td><div>" + dr[i] + "</div></td>");
        }
        sb.Append("</tr>");
      }
      sb.Append("</tbody> ");
      sb.Append("</table> ");
      return sb.ToString();
    }

  }
}
