﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StatisticsSystem.Model
{
  class ENCO_ZBBMB_HZ
  {
    #region Model
    private string _zbbm;
    private string _sjzbbm;
    /// <summary>
    /// 
    /// </summary>
    public string ZBBM
    {
      set { _zbbm = value; }
      get { return _zbbm; }
    }
    /// <summary>
    /// 
    /// </summary>
    public string SJZBBM
    {
      set { _sjzbbm = value; }
      get { return _sjzbbm; }
    }
    #endregion Model
  }
}
