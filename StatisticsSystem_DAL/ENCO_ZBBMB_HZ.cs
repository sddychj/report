﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using DBUtilityOdp;
using System.Collections.Generic;

namespace StatisticsSystem.DAL
{
  public partial class ENCO_ZBBMB_HZ
  {
    public bool Deletebbbm(string bbbm)
    {

      StringBuilder strSql = new StringBuilder();
      strSql.Append("delete from ENCO_ZBBMB_HZ ");
      strSql.Append(" where bbbm=:bbbm  ");
      OracleParameter[] parameters = {
					new OracleParameter(":bbbm", OracleDbType.Varchar2,20)			};
      parameters[0].Value = bbbm;

      int rows = DbHelperOra.ExecuteSql(strSql.ToString(), parameters);
      if (rows > 0)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
  }
}
